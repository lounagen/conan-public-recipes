from conans import ConanFile, CMake, tools


class QiFrameworkConan(ConanFile):
    name = "qiframework"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = (
        "Meta-package made to have qicc bundled with all dependencies"
        "so it can be called for code generation"
    )
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = (
        'qilang/0.1@liberforce/testing',
        'libqi/0.1@liberforce/testing',
    )

    def imports(self):
        self.copy("*", "bin", "bin")
        self.copy("*.so", "lib", "lib")
        self.copy("*.so.*", "lib", "lib")
        self.copy("*.dll", "", "lib")
        self.copy("*.dylib", "", "lib")

    def package(self):
        self.copy("*", dst="", src="", keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ["qilang", "qi"]
