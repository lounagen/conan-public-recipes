import os.path

from conans import ConanFile, CMake, tools


class QiLangConan(ConanFile):
    name = "qilang"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = (
        'qilang is a project to generate strongly-typed proxies in client'
        ' code when using the qi library.'
    )
    settings = "os", "compiler", "build_type", "arch", "cppstd"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = (
        'libqi/0.1@liberforce/testing',
        'boost/1.64.0@hoxnox/testing',
        'gtest/1.8.0@bincrafters/stable',
        'bison/3.0.4@sztomi/testing',
        'flex/2.6.3@sztomi/testing',
    )
    patch_dir = 'patches'
    exports_sources = os.path.join(patch_dir, '*.patch')
    # FIXME: build_requires = 'qibuild'

    def source(self):
        self.run("git clone https://github.com/aldebaran/qilang.git")
        self.run("cd qilang && git checkout master")

        # Apply patches
        patch_names = [
            '0001-Fix-dependency-on-specific-flex-versions.patch',
            '0002-lower-qicc-s-dependenc-on-qi-path.conf.patch',
        ]
        for patch_name in patch_names:
            patch = os.path.join(self.patch_dir, patch_name)
            tools.patch(base_path='qilang', patch_file=patch)

        # This might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly
        tools.replace_in_file(
            'qilang/CMakeLists.txt',
            'project(QiLang)',
            (
                'project(QiLang)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def configure(self):
        # Won't build unless we force the C++ standard
        if self.settings.compiler != 'Visual Studio':
            self.settings.cppstd = 'gnu11'

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="qilang")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["qilang"]
