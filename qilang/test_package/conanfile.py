import os
import os.path

from conans import ConanFile, CMake, tools


class QiLangTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("qicc", dst="bin", src="bin")
        self.copy("test_qilang", dst="bin", src="bin")

        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='lib', src='lib')

    def test(self):
        if not tools.cross_building(self.settings):
            os.chdir("bin")
            #self.run(os.path.join('.', 'test_qilang'))
            #self.run('{cmd} --help'.format(cmd=os.path.join('.', 'qicc')))

            # To test libqilang, we could use that binary
            # self.run(os.path.join('.', 'example'))
