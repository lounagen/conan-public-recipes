from conans import ConanFile, CMake, tools


class LibAlmathConan(ConanFile):
    name = "libalmath"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = "Math library to work with the Motion module from NAOqi"
    settings = "os", "compiler", "build_type", "arch", "cppstd"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = (
        'libqi/0.1@liberforce/testing',
        'qilang/0.1@liberforce/testing',
        'boost/1.64.0@hoxnox/testing',
        'gtest/1.8.0@bincrafters/stable',
        'eigen/3.3.4@conan/stable',
        )
    # FIXME: build_requires = 'qibuild'

    def source(self):
        self.run("git clone https://github.com/aldebaran/libalmath.git")
        self.run("cd libalmath && git checkout master")

        # This might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly
        tools.replace_in_file(
            'libalmath/CMakeLists.txt',
            'project(ALMATH)',
            (
                'project(ALMATH)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def configure(self):
        # Won't build unless we force the C++ standard
        if self.settings.compiler != 'Visual Studio':
            self.settings.cppstd = 'gnu11'

    def build(self):
        cmake = CMake(self, parallel=False)
        cmake.definitions['ALMATH_WITH_QIGEOMETRY'] = False
        cmake.definitions['WITH_PYTHON'] = False
        cmake.verbose = True
        cmake.configure(source_folder="libalmath")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["almath"]
