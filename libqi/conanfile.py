from conans import ConanFile, CMake, tools


class LibQiConan(ConanFile):
    name = "libqi"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = "Package for the libqi library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    requires = (
        'boost/1.64.0@hoxnox/testing',
        'gtest/1.8.0@bincrafters/stable',
        'OpenSSL/1.1.0g@conan/stable',
        'zlib/1.2.11@conan/stable',
    )
    # FIXME: build_requires = 'qibuild'

    def source(self):
        self.run("git clone https://github.com/aldebaran/libqi.git")
        self.run("cd libqi && git checkout team/platform/dev")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file(
            'libqi/CMakeLists.txt',
            'project(LibQi)',
            (
                'project(LibQi)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def configure(self):
        # Required because source package was not built with -fPIC
        self.options['boost'].shared = True
        # Won't compile otherwise
        self.options['gtest'].shared = True

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="libqi")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["qi"]
